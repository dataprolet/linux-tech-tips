## Random software recommendations

- Mobile browser: Mull
- Deduplication: Czkawka
- Ventoy
- Nala
- https://wiki.archlinux.org/title/LibreOffice#Grammar_checking
- Media sorting: phockup
- Backup: Borg
- Firmware update: fwupd
- Analyse pishing mails: ThePhish
- OCR: OCRmyPDF
- Upscal images: Upscaly
- Send files: croc
- Hosting: pgs.sh
- LLM: ollama
- VPN: Tailscale