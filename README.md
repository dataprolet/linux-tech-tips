## This is a collection of various, random things.

Moved to https://git.dataprolet.de/dataprolet/Linux-Tech-Tips.

- [Linux Tips](https://gitlab.com/dataprolet/linux-tech-tips/-/blob/main/linux-tips.md): _Mostly for beginners._
- [Recommended software](https://gitlab.com/dataprolet/linux-tech-tips/-/blob/main/software.md) (WIP)
- [More random tips](https://gitlab.com/dataprolet/linux-tech-tips/-/blob/main/really-random-tips.md) (WIP)
- [Collection of scripts](https://gitlab.com/dataprolet/linux-tech-tips/-/tree/main/Scripts): _Useful scripts, more to come._
- [Various random tutorials](https://gitlab.com/dataprolet/linux-tech-tips/-/tree/main/Tutorials): _More in depth tutorials for various things._