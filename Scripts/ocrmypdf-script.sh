#!/bin/bash
echo "Please select a file to OCR:"
IFS=$'\n'
files=( $(ls | grep pdf) )
declare -a files
select file in ${files[*]}
do
break
done
ocrmypdf -l deu+eng -d -c $file "${file%.*}"-OCR.pdf
