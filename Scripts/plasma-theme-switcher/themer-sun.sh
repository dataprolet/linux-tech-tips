#!/bin/bash

# Required the program 'jq'.

# Get your coordinates here: https://www.latlong.net/
lat=52.520008
long=13.404954

# Get the times for sunrise and sunset
sunrise=$(curl "https://api.sunrisesunset.io/json?lat=$lat&lng=$long" |jq -r '.results | .sunrise')
sunset=$(curl "https://api.sunrisesunset.io/json?lat=$lat&lng=$long" |jq -r '.results | .sunset')

# Convert them into hours and minutes
hour_sunrise=$(date -d "$sunrise" +"%H")
min_sunrise=$(date -d "$sunrise" +"%M")
hour_sunset=$(date -d "$sunset" +"%H")
min_sunset=$(date -d "$sunset" +"%M")

# Get the current hour and minute
current_hour=$(date +%H)
current_minute=$(date +%M)

# Convert current time to minutes since midnight
current_time=$((10#$current_hour * 60 + 10#$current_minute))

# Define the start times in minutes since midnight
start_command1=$((10#$hour_sunset * 60 + 10#$min_sunset))
end_command1=$((10#$hour_sunrise * 60 + 10#$min_sunrise))

if (( current_time >= start_command1 || current_time < end_command1 )); then
    # Execute command1 if the time is between sunset and sunrise
    plasma-apply-colorscheme BreezeDark
else
    # Execute command2 if the time is between sunrise and sunset
    plasma-apply-colorscheme BreezeLight
fi
