#!/bin/bash

# This script uses the unsupported installation method descripted here:
# https://docs.brew.sh/Installation#untar-anywhere-unsupported

# This makes it possible to run this script as root and non-interactive.

# Script must be run as root
if [ "$(id -u)" != "0" ]; then
   echo "This script must be run as root. Exiting.." 1>&2
   exit 1
fi

# Define current user and home directory
user=$(stat -f%Su /dev/console)
HOME=/Users/$user

# Create homebrew directory in /opt
cd /opt
mkdir ./homebrew

# Download and extract homebrew
curl -L https://github.com/Homebrew/brew/tarball/master | tar xz --strip 1 -C homebrew

# Change ownership to current user
chown -R $user /opt/homebrew

# Update brew
sudo -u $user /opt/homebrew/bin/brew update --force --quite

# Export env
echo "export PATH=$PATH:/opt/homebrew/bin" >> "$HOME/.zshrc"

## Optional: Install XCode Command Line Tools

# Temporary file to make 'softwareupdate' list the Command Line  Tools
touch /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress

# Get the latest version of the Command Line Tools
latest=$(/usr/sbin/softwareupdate -l | grep -B 1 -E 'Command Line Tools' | awk -F '*' '/^ *\\*/ {print $2}' | sed -e 's/^ *Label: //' -e 's/^ *//' | sort -V | tail -n1)

# Install latest version
/usr/sbin/softwareupdate -i "${latest}"

# Set correct path
/usr/bin/xcode-select --switch /Library/Developer/CommandLineTools

# Delete temporary file
rm /tmp/.com.apple.dt.CommandLineTools.installondemand.in-progress

exit 0