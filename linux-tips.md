*This is a random collection of useful things I came across.  
It's mostly distro-agnostic but sometimes specific for [pacman-based distros](https://en.wikipedia.org/wiki/List_of_Linux_distributions#Pacman-based).*  

[TOC]
  
### 0. *Short example template.*  
- Description: *What is this good for.*  
- Requires: *Do you need any particular software (provide a link)?*
- How-to: *Straight forward description and maybe a code example.* 
    - *Detailled description if necessary.*  
    - `$ this is the actual code lol`  
- Aditionally: *Anything else that might be important like additional ressources.*  
  
### 1. Automatic clear cache.  
- Description: Automatically move files older than a specific amount of time from your user's `~/.cache` directory to the trash.  
- Requires: The [rmtrash](https://github.com/PhrozenByte/rmtrash) package and [anacron](https://www.youtube.com/watch?v=41Lay6ZPtRQ).  
- How-to: Add the following line to `/etc/cron.daily/cache`.  
    - `find ~/.cache/* -mtime +7 -exec rmtrash -rf {} \;`  
    - This will move files older than 7 days in `~/.cache` to the trash every day.  
- Additionally: This can also be done using regular `rm` instead of `rmtrash`.  
  
### 2. Limit systemd journal size.  
- Description: Limit the systemd journal to a specific size.  
- Requires: Specific to systems using [systemd](https://systemd.io/).  
- How-to: Edit `/etc/systemd/journald.conf` and change `SystemMaxUse=50M`.  
- Additionally: [ArchWiki: systemd](https://wiki.archlinux.org/title/Systemd/Journal#Journal_size_limit).  
- Additionally: This can also be achieved using `journalctl` and be [limited by time](https://wiki.archlinux.org/title/Systemd/Journal#Clean_journal_files_manually).  
  
### 3. Syntax highlight for nano.  
- Description: Enable syntax highlighting for nano.
- Requires: The [nano](https://www.nano-editor.org/) editor.  
- How-to: Edit `~/.config/nano/nanorc` or to `/etc/nanorc` and add the following:
    - `include "/usr/share/nano/*.nanorc"`.
- Additionally: [ArchWiki: Nano](https://wiki.archlinux.org/title/Nano#Syntax_highlighting).  
  
### 4. Repeat a command using watch.
- Description: Repeat and watch a command in a custom interval.  
- Requires: The [procfs](https://gitlab.com/procps-ng/procps) package.
- How-to: Use `watch` followed by your command.  
    - Example: `$ watch -n 1.0 "du -h -s ./$folder`  
    - This prints the summarized size of $folder in human-readable format every second.  
- Additionally: ---

### 5. Repeat last command as su.  
- Description: Repeat the last command using sudo.  
- Requires: The [sudo](https://www.sudo.ws/) package.  
- How-to: Just type `$ sudo !!` and the last command will be repeated using `sudo`.
- Additionally: ---
  
### 6. Prevent overwriting files.  
- Description: Use alias for `rm`, `cp` and `mv`.  
- How-to: Add the following alias into your `~/.bashrc`.  
    - Use *-interactive* option to get asked before overwriting files.  
    - `alias mv="mv -i"`  
- Additionally: [ArchWiki: Core utilities](https://wiki.archlinux.org/title/Core_utilities#Preventing_data_loss).  
  
### 7. Auto-clean package cache.  
- Description: Automatically clean the pacman package cache using paccache.
- Requires: A [pacman-based distro](https://en.wikipedia.org/wiki/List_of_Linux_distributions#Pacman-based) and the [pacman-contrib](https://github.com/archlinux/pacman-contrib) package.
- How-to: Enable the paccache.timer.  
    - `$ systemctl enable paccache.time`  
- Additionally: Edit the paccache.service in /usr/lib/systemd/system/ and change the command to e.g. keep the latest installed version and no uninstalled version of all packages: `ExecStart=/usr/bin/paccache -rk1 && /usr/bin/paccache -ruk0`  
  
### 8. Passwordless SSH.  
- Description: Connect to a remote maschine using SSH without a password.  
- Requires: A SSH implementation like [TinySSH](https://tinyssh.org/).  
- How-to: Generate a keypair and send the public key to the remote maschine's authorized keys.    
    - Generate a keypair: `$ ssh-keygen -t rsa`  
    - Send the key: `$ ssh-copy-id $remote-username@$remote-ip-address`
    - Fill in the username and IP address of your remote maschine.
    - For example: `$ ssh-copy-id foo@192.168.178.123`
- Additionally: Here's a more detailed [tutorial](https://www.hostinger.com/tutorials/how-to-setup-passwordless-ssh/).
  
### 9. Automatic clean orphaned packages.  
- Description: Automatically remove orphaned packages.  
- Requires: A pacman-based distribution and anacron.  
- How-to: Add the following line to `/etc/cron.daily/orph`.  
    - `pacman -Qtdq | pacman -Rns -`  
- Additionally:  
  
### 10. Automatic retrieve latest mirrors.
- Description: Automatically retrieve the latest Arch Linux mirrors and write them to the mirrorlist.  
- Requires: An Arch-based distro and [reflector](https://xyne.dev/projects/reflector/).  
- How-to: Install reflector and enable the reflector.timer.  
    - `$ systemctl enable reflector.timer`  
- Additionally: [ArchWiki: Reflector](https://wiki.archlinux.org/title/Reflector#systemd_timer).  
  
### 11. Extract text containing keywords from any file.
- Description: This command will filter and sort text based on keyword from any file(s).  
- Requires: [GNU Coreutils](https://www.gnu.org/software/coreutils/), [GNU grep](https://www.gnu.org/software/grep/)  
- How-to:  
    - `$ cat ./*/files* | grep -e keyword1 -e keyword2 | sort -u | nl > file`
    - `cat` will take any file(s), multiple files in multiple subdirectories can be specified.  
    - `grep -e` will look for any and/or all specified keywords.
    - `sort -u` will sort the results alphabetically and remove duplicates.  
    - `nl` will add a number in front of every result (optionally).  
    - `>` will write everything in a specified file.
- Additionally: This is simply an example of how useful the coreutils can be, especially when used together.

### 12. Enable parallel downloads for pacman.  
- Description: Enable parallel downloads for faster updates with pacman.  
- Requires: A [pacman-based distro](https://en.wikipedia.org/wiki/List_of_Linux_distributions#Pacman-based).  
- How-to: Edit pacman's config-file at `/etc/pacman.conf` and uncomment `ParallelDownloads` and add a number of parallel downloads like 5 or 10.
    - `# nano /etc/pacman.conf`
- Additionally: [ArchWiki: Pacman](https://wiki.archlinux.org/title/Pacman#Enabling_parallel_downloads).

### 13. Enable smartd service.
- Description: Enable the smartd daemon to monitor SMART status and emit notifications to log.
- Requires: [smartmontools](https://archlinux.org/packages/?name=smartmontools)
- How-to:
    - Enable the service `# systemctl enable smartd.service`
    - Uncomment/add the following line to `/etc/smartd.conf`:
        `DEVICESCAN -a`
- Additionally: [ArchWiki: S.M.A.R.T.](https://wiki.archlinux.org/title/S.M.A.R.T.#smartd)